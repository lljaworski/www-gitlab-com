---
layout: job_family_page
title: "IT Compliance Manager"
description: "As an IT compliance manager, you will assist in the assessment of technology-related compliance issues across the organization." 
---

As an IT compliance manager, you will assist in the assessment of technology-related compliance issues across the organization including information security, identity management, user access, and data integrity. This includes working with systems owners and administrators to identify, document and monitor current risks and controls.

## IT Compliance Manager

### Job Grade

The IT Compliance role is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Be the main point of contact for IT and assist on all internal and external audit teams where IT inquiry is required
* Monitor activities of assigned IT areas to ensure compliance with internal policies and procedures including monthly, quarterly, and annual account and activity reviews
* Ensure execution of required testing and auditing activities for the IT Department by internal and external parties leading to successful certification of the company on an ongoing basis
* Triage Change Management issues and provide recommendations
* Work collaboratively with Security Compliance and Legal teams to identify and manage privacy, data protection risks, and compliance requirements to help meet stakeholder expectations
* Make broad recommendations on improving compliance related processes and/or procedures as it pertains to the IT department
* Partner with management, business teams, and/or data team to implement solutions

### Requirements

* BA/BS in a business related field and/or equivalent years of education and experience working in a related field
* 3-5 years experience in Information Technology or Information Security experience.
* Certified Information Systems Security Professional (CISSP) preferred
* Knowledge of policies and procedures related to GDPR, CCPA, and PCI
* Excellent interpersonal, verbal, and written communication skills with the ability to communicate compliance related concepts to a broad range of technical and non-technical staff
* Successful experience working, collaborating, and establishing credibility and relationships with senior leadership, colleagues, and clients
* Demonstrated success working with internal audit, external auditors, outside consultants, and legal affairs
* Demonstrated experience leading large-scale projects
* Ability to use GitLab

## Senior IT Compliance Manager

### Job Grade

The IT Compliance role is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
All the responsibilities of a Business Systems Analyst, plus:

* Develop, maintain, and publish the Business Continuity and Disaster Recovery plan with a supporting Business Impact Analysis report and assist with customer audit inquiries
* Identify remediating controls and performance gaps compared to industry best practice to help gain stakeholder buy-in
* Assist organizations in the identification and management of IT security risks by assessing the current state
* Prioritize improvements and conduct compliance projects to reduce risk and improve regulatory compliance
* Create Compliance frameworks in issue templates and updating handbook with relevant procedural information

### Requirements
This role includes all of the requirements above, plus:

* Extensive knowledge and understanding of audit standards and practices, and control frameworks
* Extensive knowledge and understanding of information security policies, standards, and guidelines
* Solid knowledge and understanding of end-user computing tools, hardware, application software, network, communications, and mobile technologies
* Ability to use GitLab
* Solid knowledge and understanding of concepts and philosophies regarding the design and deployment of information technologies and associated architectural concepts, principles, and tools

## Staff IT Compliance Manager (Staff level is only obtainable through career development)

### Job Grade
The IT Compliance role is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

This role includes all of the responsibilities above, plus:
Mentor other IT Compliance Managers and improve quality and quantity of the team’s output
Program-level leadership across teams outside of IT Compliance
Anticipate internal and external audit requirements in advance
Keep abreast of changing regulatory requirements and appropriately adjust the scope of the IT Compliance program to accommodate these changes

### Requirements
This role includes all of the requirements above, plus:

* Identify, maintain, and publish the requirements for the IT department to achieve compliance and privacy standards including GDPR, SOX, ISO 27001 and other standards
* Proven ability to create new IT Compliance programs and deliver successful results
* Proven experience building IT Compliance programs from the ground up
* Proven experience with successful first-time internal and external audits
* Ability to use GitLab
* Detailed and comprehensive knowledge of all GitLab tools, service, and infrastructure

## Manager, IT Compliance

### Job Grade
The IT Compliance role is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Build, scale, and manage our IT Compliance team to support our needs as a distributed company
* Be the IT Compliance Expert at GitLab
* Hold regular 1:1’s with all members of the IT Compliance team
* Triage and manage priorities of the IT Compliance team
* Represent the IT Compliance team in different company functions
* Create and execute a plan to develop and mature our IT Compliance capabilities and Infrastructure
* Collaborate with all functions of the company to ensure IT Compliance needs are addressed
* This position reports to the VP of Information Technology

### Requirements

This role includes all of the requirements above, plus:

* Experience working with leadership to execute on IT Compliance processes and procedures
* Contribute to and enable GitLab’s operational strategy by enabling distributed asynchronous operations while ensuring compliance with GDPR, SOX, ISO 27001, and other standards
* Ability to use GitLab
* Experience building and maintaining corporate IT Compliance policies and processes

## Performance Indicators (PI)

*  [Average Issues](/handbook/business-ops/metrics/#average-issues)
*  [Average Merge Requests](/handbook/business-ops/metrics/#average-merge-request)
*  Evaluate compliance of IT tools or processes
*  Evaluate changes to IT tools and processes based on risk
*  Provide more detailed and more practical guidance to the organization with the goal of improving compliance related processes and/or procedures.

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Senior Director, Enterprise Applications
* Candidates will then be invited to schedule a third interview with our VP, IT
* Then the candidate will be invited to interview with the Director of Risk and Compliance

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Career Ladder

The next step in the IT Compliance job family is to move to the [TODO]() job family.
