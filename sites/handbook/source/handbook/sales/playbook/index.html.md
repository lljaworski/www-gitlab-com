---
layout: handbook-page-toc
title: "Enterprise Sales"
description: "The Enterprise Sales department at GitLab focuses on delivering maximum value to strategic and large prospects and customers throughout their entire journey with GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Overview 
Welcome to the Enterprise Sales handbook page!

The Enterprise Sales department is part of [GitLab Sales](https://about.gitlab.com/handbook/sales/#welcome-to-the-sales-team-homepage). The sales field in Enterprise is made up of [Strategic Account Leaders (SALs)](https://about.gitlab.com/job-families/sales/strategic-account-leader/) who work across functions deliver maximum value to strategic and large prospects and customers throughout their entire journey with GitLab.

Besides this page, there are a few bookmarks you’ll want to set that will be your main sources of truth during your everyday work. These are:

- **[The GitLab Sales handbook page](https://about.gitlab.com/handbook/sales/#welcome-to-the-sales-team-homepage):** This page serves as our home base. From here, you can find a wealth of resources that are relevant to the entire sales field. Whether you’re looking for sales order processing info, or wondering how to get executive support, this is where you’ll find what you need.
- **[Field Slack Channels](https://about.gitlab.com/handbook/sales/sales-google-groups/#field-slack-channels):** View and join these Slack channels based on your role and team to stay connected with your peers.
- **[The SAL learning hub](https://gitlab.edcast.com/channel/strategic-account-leaders):** You’ll receive access to EdCast during your first week of onboarding. Follow the this learning channel in EdCast to see all training relevant to your role in one place. The SAL learning hub includes training and refresher courses on our sales methodology, tools, and sales skills. 
- **[Highspot](https://gitlab.highspot.com/spots/615dd82071cff4c4b2bcbc32?list=616f308990bdabb28621ca57&overview=true):** You’ll receive access to Highspot in Okta during your first week of onboarding. It’s where all sales content is stored - whether you’re looking for competitive intelligence, solution selling guidance, templates to use in proposals, or the latest marketing assets - it’s all there.

## The SAL playbook
[The SAL playbook](https://docs.google.com/presentation/d/1S-8Jvr-NcMAEjqB5j0i53WZg4aEtTGTicCpWbZ9JaNk/edit#slide=id.g10c6ab6d93e_0_318) is how a typical, high-performing SAL runs their business on an annual, quarterly, monthly, and weekly basis. Use it as a guide to understand the basics of what key activities and collaboration points are required for things to run smoothly in your territory. This includes collaborating with your deal team.

##### Sales planning
Every Strategic Account Leader in Enterprise Sales should have a plan for how they're approaching their patch. The first step is to [create a sales territory plan](https://about.gitlab.com/handbook/sales/territory-planning/#overview): a collaborative, workable plan for targeting the right customers and implementing goals for income and consistent sales growth over time. It’s usually done annually and updated throughout the year. Your MVP territory plan will help you prioritize your accounts and is then followed by creating [account plans](https://about.gitlab.com/handbook/sales/account-planning/) for those accounts. View resources and training in the [SAL Learning Hub](https://gitlab.edcast.com/channel/strategic-account-leaders).

##### Moving a deal through the pipeline
The below table shows major strategic resources that can help you be successful during each milestone of a deal - from prospecting to transitioning to a the post-sales team. For operational resources, head to the general sales page up top. For commonly used sales assets like marketing plays and pitch decks, head to the [marketing resources handbook page](https://about.gitlab.com/handbook/marketing/strategic-marketing/sales-resources/).


| Educate & Engage<br>(Sales Stages 0-3) | Facilitate the Opportunity<br>(Sales Stages 3-4) | Deal Closure<br>(Sales Stages 5-6) | Retain and Expand<br>(Growth) |
| ------ | ------ | ------ | ------ |
| * [Prospecting](/handbook/sales/prospecting/)<br> * [Discovery](/handbook/sales/playbook/discovery/)<br> * [Articulate Value](https://about.gitlab.com/handbook/sales/command-of-the-message/) | * [Position to Win](https://about.gitlab.com/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/#step-2-scoping)<br> * [Technical Evaluation](https://about.gitlab.com/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/#step-3-technical-evaluation)<br> * [Customer-Centric Proposal](https://about.gitlab.com/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/#12-creating-the-customer-deck-phase-1) | * [Mutual Close Plan](/handbook/sales/mutual-close-plan)<br> * [Negotiate to Close](/handbook/sales/negotiate-to-close/)<br> * [Order Processing](/handbook/sales/field-operations/order-processing/) | * [Account Transition](/handbook/customer-success/pre-sales-post-sales-transition/)<br> * [Customer Onboarding](/handbook/customer-success/tam/onboarding/) |

As you move a deal through the pipeline, use the [Opportunity Stages](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#opportunity-stages) guidance to help you validate your opportunity and forecast it correctly. This guidance gives definitions for each stage, tells you who might be involved on your team, what activities are typically done in each stage, and what is required before you can move it further in the pipeline.

## Quick links to common tools

If you don’t have access to the items below and believe you should, open an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request).

**Executing your strategy**

- [Gainsight](https://about.gitlab.com/handbook/sales/gainsight/) is used for documenting account details, timelines, and objectives
- [ZenDesk](https://about.gitlab.com/handbook/support/workflows/zendesk-overview.html) is our ticketing system. Access is usually provided during onboarding. Ask your manager if you need access to ZenDesk.
- [LucidChart](https://apis.google.com/additnow/l?applicationid=7081045131&__ls=ogb&__lu=https%3A%2F%2Flucid.app%2Fusers%2FgoogleLogin%3Fdomain%3Dgitlab.com) for creating diagrams and org charts for account plans.
- [LinkedIn Sales Navigator](https://www.linkedin.com/business/sales/blog/sales-navigator/what-is-linkedin-sales-navigator) is one of the main prospecting tools we use at GitLab. It features a powerful set of search capabilities, improved visibility into extended networks, and personalized algorithms to help you reach the right decision maker. All SALs have access to LSN. [Click to go to the EdCast training.](https://gitlab.edcast.com/insights/learning-linkedin-sales) And view additional videos and tutorials on the [handbook page](https://about.gitlab.com/handbook/sales/training/social-selling/).
- [Chorus](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/go-to-market/chorus/) is used to view recordings of GitLab sales calls and demos.
- [Account-based marketing (ABM)](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/#account-based-marketing-abm) uses [DemandBase](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/) to develop account-based strategy campaigns, create focused account lists, and push out and measure advertising to targets. You can view this data with them or with your Sales Development Representative (SDR).
- [Salesforce Inbox](https://www.salesforce.com/products/sales-cloud/features/crm-email-connector/?d=cta-conf-1) is a productivity tool to integrate Salesforce with emails. You may need to request access. View the 
- [Outreach.io](https://university.outreach.io/page/prospector-learning-track) is a tool used to automate emails in the form of sequences. Users can track open rates, click through rates, response rates for various templates and update sequences based on these metrics. Outreach.io also helps to track sales activities such as calls. All emails/calls/tasks that are made through Outreach.io are automatically logged in Salesforce with a corresponding disposition. 
- [Conversica](https://about.gitlab.com/handbook/marketing/marketing-operations/conversica/) is a conversational AI tool that helps enterprise marketing, sales, customer success, and finance teams attract, acquire and grow customers at scale across the customer revenue lifecycle. The AI Assistant works by engaging the prospect in a human-like conversation over email in an effort to further qualify the prospect.

**Understanding your business**
- Salesforce (View in Okta)
- [Clari](https://about.gitlab.com/handbook/sales/forecasting/) is used to view and document forecasting and pipeline data.
- [Periscope](https://app.periscopedata.com/app/gitlab/403199/Welcome-Dashboard-%F0%9F%91%8B) is a data visualization tool. Your manager can send you specific dashboards relevant to your team to view.
- [DemandBase](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/) is used by SDRs who can show you relevant data related to your target prospects. It has a wealth of intent and propensity to buy data that is married with account engagement indicators to create a holistic intent mapping for each account.
- [Datafox](https://about.gitlab.com/handbook/business-ops/tech-stack/#datafox) is used to view relevant account information.
- [LeanData](https://about.gitlab.com/handbook/marketing/marketing-operations/leandata/) is automatically pulled into Salesforce as data that you can use to better understand contacts and accounts. You don’t need to request access.

**Understanding your customers current product usage**
- [Product Usage Data in Gainsight](https://about.gitlab.com/handbook/customer-success/product-usage-data/using-product-usage-data-in-gainsight/#using-product-usage-data-in-gainsight) is used by your TAM and can provide you with insights into current customer usage data by use case by walking you through the relevant dashboard in Gainsight.
- [CustomersDot Admin](https://customers.gitlab.com/admin/) (see [overview video here](https://youtu.be/G9JuHXqV5LM)): most customers have an account that breaks down their purchases and provides some insight into SaaS usage. Also good for linking purchases to VersionDot usage data
- [LicenseDot](https://about.gitlab.com/handbook/sales/field-operations/sales-systems/license-usage-app/#how-to-use-licensedot) lists all self-managed licenses issued. Also links to VersionDot to tie a license/subscription to a server and its usage.

View the [Commercial Sales Playbook](https://about.gitlab.com/handbook/sales/commercial/) here.
