title: Curve
file_name: Curve
canonical_path: /customers/curve/
cover_image: /images/blogimages/curve_cover_image.jpg
cover_title: |
  Fintech innovator Curve counts on the GitLab platform
cover_description: |
  Improved CI/CD, security scans, project management, and auditability drive application innovation
twitter_image: /images/blogimages/curve_cover_image.jpg
twitter_text: Learn how Fintech innovator Curve counts on the GitLab platform to drive innovation @imaginecurve
customer_logo: /images/case_study_logos/curve_logo.jpeg
customer_logo_css_class: brand-logo-tall
customer_industry: Financial Services
customer_location: London, United Kingdom 
customer_solution: GitLab Self Managed Ultimate
customer_employees: |
  500
customer_overview: |
  Innovation is the lifeblood of the young fintech industry. Software engineering needs to be especially fleet of foot. At startup Curve, developers chose GitLab to meet these pressing needs. 
customer_challenge: |
  Streamline launches of critical fintech services, meet stringent security and audit requirements, and improve developer productivity.
key_benefits: >-

  
    DevOps platform as a single application

  
    Built-in security features 

  
    Agile project management

  
    PCI Compliance 

  
    Using GitOps (Infrastructure as code)


    Toolchain simplification


    Huge cost savings 


    Quick developer onboarding and adoption
customer_stats:
  - stat:  5 tools
    label: GitLab replaced Bitbucket, GitHub, Circle CI, Quay.io and Jenkins
  - stat:  40,000
    label: 40k appx annual cost savings from moving to GitLab 
  - stat:  50%
    label: 50% reduction in average CI pipeline duration
customer_study_content:
  - title: the customer
    subtitle: Fintech Innovator 
    content: >-
  
       [Curve](https://www.curve.com/usa/) is a fintech platform that has gone live in over 31 markets across the UK and European Economic Area and is imminently expanding into the US. Curve combines your credit cards, debit cards, and loyalty cards in one. You can even add receipts. So it’s the only card you need to carry. And the only pin you need to remember. Also known as over-the-top payment servicing, this new model of financial and payment services is enabled by Curve’s commitment to product innovation through advanced software engineering.


  - title: the challenge
    subtitle:  Fragmented toolchain reducing productivity and results 
    content: >-
    

        The early days of programming at Curve saw team members working mainly in PHP.  As efforts advanced, teams evolved to embrace new languages, including Golang, Kotlin, Swift, and NodeJS. However, a fragmented toolchain led to difficult integration of software components, and poor visibility into development processes. There was a black box element to builds that made it difficult to identify and fix issues. Resource overhead required for administering and supporting a wide variety of software tools threatened teams’ abilities to meet productivity objectives. Pressures were further heightened by the need to ensure optimal security, Payment Card Industry (PCI) compliance, and reporting in fintech – which is a high-profile industry with continually evolving regulations.
 


        The task of streamlining DevOps did not become less difficult as additional developers joined. Curve looked to simplify development and streamline operations to support the growth of their team. “It was a battle to enable developers to actually do their jobs efficiently. The obvious choice was to have everything in one place and well-contained on a single pane of glass,” said Ed Shelton, Site Reliability Engineer at Curve.



  - title: the solution
    subtitle:  One DevOps platform that ensures compliance and increases efficiency
    content: >-


        Curve engineers used GitLab to attain standardized continuous integration and continuous delivery (CI/CD) pipeline processes. As part of that, the team has gained greater control of templating issues that had stymied progress. Among the early advantages achieved with GitLab’s implementation was the ability to quickly onboard new engineers, simply by granting them access to GitLab and getting them logged in. No need to create accounts for 5+ services and generate personal access tokens for each one - just use GitLab. Developers were “good to go” on their first day.


      
        Improvements in security included use of GitLab’s security scanning for quick visibility into vulnerabilities in both new and older code.  Reports now detect vulnerabilities in individual jobs automatically, and prevent risky changes from inadvertently making it to production. Meanwhile, maintenance time devoted to CI/CD has dramatically reduced. New releases can be quickly rolled out or rolled back. New initiatives can be taken on quickly without worrying about dedicating developer resources to gluing together disparate systems. 

      

  - title: the results
    subtitle:  DevOps Platform delivering better code and empowering developers 
    content: >-

        Use of GitLab Ultimate at Curve has vastly improved CI/CD efforts, garnered significant security benefits, eased templating, and streamlined project management and auditability. Builds that run more quickly have been beneficial, allowing programmers to focus on programming. With GitLab, Curve teams effortlessly achieved PCI compliance. GitLab dashboards now highlight vulnerabilities that could compromise security. Along with this come advances in templating that support standardization and greater governance for the organization. The result has been highly performant pipeline operations, and reduced time-to-release. Curve teams have seen an appx  50% reduction in the time it takes for pipeline deployment. GitLab’s benefits as a project management tool are several. It is used for the full development life cycle – covering all the repositories and the software development itself, including product development, code reviews, and tracking external partners as well. Boards, milestones, and epics are particularly useful in the context of fintech requirements. Platform capabilities support creation of audit trails that mark activity on services, noting where a change comes from, how the change manifested, and how changes are actually implemented. Being able to track how services evolve is crucial in this industry. Previously, finding out how something made it into production was difficult. 
        
        
        
        By having the full software development lifecycle visible in one place, Curve has clear visibility into how developers are working and where the bottlenecks may be. The ability to roll out a new feature with agility, or just do a comprehensive bug search, has been a big win for modernization. Where there has been a need to roll out state-wide changes, teams have been able to employ the GitLab API and make these changes easily and effectively. Finally, the GitLab open-source ethos maps closely with Curve’s development philosophy. GitLab community forums always provide a preview of upcoming features and their status, and such transparency aids in planning, engineer Ed Shelton indicated. The comprehensive documentation resources empower developers to self-serve, and frees up their time to focus on code. GitLab supports this fintech innovator’s rapid growth with full compliance and increased operational efficiency.




   

        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: Before we moved to GitLab there was a big burden on operation teams. It was a battle to enable developers to effectively do their jobs. The obvious choice was to have everything in one place and well-contained through a single pane of glass
    attribution: Ed Shelton
    attribution_title: Site Reliability Engineer, Curve












